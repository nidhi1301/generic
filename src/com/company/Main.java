package com.company;

public class Main {
    public static void main(String[] args) {

        GenericClass<Integer> obj = new GenericClass(8);
        System.out.println(obj.printValue());

        Character[] charArray = {'a', 'b', 'c', 'd', 'e'};
        obj.printArray(charArray);
    }
}
