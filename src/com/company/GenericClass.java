package com.company;

public class GenericClass<dataType> {
    dataType value;

    public GenericClass(dataType value) {
        this.value = value;
    }

    public dataType printValue() {
        return value;
    }

    public <anotherDataType> void printArray(anotherDataType[] inputArray) {
        for (anotherDataType element : inputArray) {
            System.out.println(element);
        }
    }


}



